import { Circle } from './classes/circle';
import { Ball } from './classes/ball';
import { Shape } from './interfaces/shape';
import { Balloon } from './classes/balloon'
import { Constants } from './constants';
import { Input } from './input';
import { Utils } from './classes/utils'

let canvas: HTMLCanvasElement;
let ctx: CanvasRenderingContext2D;
let shape: Shape = new Ball(20, 0, 80, 'blue', 0.6);
let frameCount = 0;
let particle1: Shape = new Balloon(200, 10, Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle2: Shape = new Balloon(100, 120, Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle4: Shape = new Balloon(150, 330, Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle5: Shape = new Balloon(200, 430, Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle6: Shape = new Balloon(300, 500, Utils.randomIntFromInterval(20, 60), 'green', 0.6);

let particleList: Shape[] = [];

function gameLoop() {
    requestAnimationFrame(gameLoop);
    ctx.fillStyle = Constants.SCREEN_BASE_CLOUR;
    ctx.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
    resolveCollisions(particleList);
    updateParticles(particleList);
    drawParticles(particleList, ctx);
    shape.updatePosition(Constants.FPS); //
    shape.draw(ctx);
    frameCount++;
}

window.onload = () => {

    particleList.push(particle1);
    particleList.push(particle2);
    // particleList.push(particle3);
    particleList.push(particle4);
    particleList.push(particle5);
    particleList.push(particle6);

    canvas = <HTMLCanvasElement>document.getElementById('cnvs');
    ctx = canvas.getContext("2d");
    document.addEventListener('keydown', function(event){
        Input.keyboardInput(event, shape);
    });
    frameCount = 0;
    gameLoop();
}

function resolveCollisions(particleList: Shape[]) {
    //debugger;
    for (let i = 0; i < particleList.length; i++) {
        for (let j = i + 1; j < particleList.length; j++) {
            if (particleList[i].isColliding(particleList[j])) {
                particleList[i].resolveCollision(particleList[j]);
                //console.log('p:' + i + ', x: ' + particleList[i].position.x + ', y: ' + particleList[i].position.x + ', vX: ' + particleList[i].position.x + ', vY: ' + particleList[i].position.x);
                
            }
        }
    }
}

function updateParticles(particleList: Shape[]) {
    for ( let shape of particleList) {
        shape.updatePosition(Constants.FPS);
    }
}

function drawParticles(particleList: Shape[], ctx: CanvasRenderingContext2D) {
    for ( let shape of particleList) {
        shape.draw(ctx);
    }
}