import { Shape } from './interfaces/shape';

export class Input {
    public static keyboardInput(event: KeyboardEvent, shape: Shape) {
        // PRESS LEFT ARROW OR 'A' KEY
        if (event.keyCode == 37 || event.keyCode == 65) {
            shape.velocity.x -= 5;
        }
        // PRESS UP ARROW OR 'W' KEY
        else if (event.keyCode == 38 || event.keyCode == 87) {
            shape.velocity.y -= 5;
        }
        // PRESS RIGHT ARROW OR 'D' KEY
        else if (event.keyCode == 39 || event.keyCode == 68) {
            shape.velocity.x += 5;
        }
        // PRESS DOWN ARROW OR 'S' KEY
        else if (event.keyCode == 40 || event.keyCode == 83) {
            shape.velocity.y += 5;
        }
    }
}