import { Shape } from '../interfaces/shape'
import { Vector2D } from './vector2d' 
import { PhysicsConstants } from '../physics.constants'
import { Constants } from '../constants'
import { Utils } from './utils'

export class Balloon implements Shape {

    public radius: number = 10;
    public mass: number;
    public lineWidth: number = 2;
    public color: string = "red";
    public position: Vector2D;
    public velocity: Vector2D;
    public deformation: Vector2D;
    constructor(x: number, y: number, radius: number, color: string = "red", line_width: number = 2)
    {
       this.position = new Vector2D(x, y);
       this.velocity = new Vector2D(Utils.randomIntFromInterval(1, 20), Utils.randomIntFromInterval(1, 20));
       this.mass = this.findVolume() * PhysicsConstants.RHO_HELIUM;
       this.radius = radius;
       this.color = color;
       this.lineWidth = line_width;
    }

    public draw(ctx: CanvasRenderingContext2D) {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = this.color;
        ctx.lineWidth = this.lineWidth;
        ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.restore();
    }

    public updatePosition(dt: number) {

        //this.velocity.y +=  PhysicsConstants.G * dt;
        //debugger;
        let dragX = this.findDragX();
        let dragY = this.findDragY();
        this.position.x += (this.velocity.x * dt);// - this.findDragX();
        this.position.y += (this.velocity.y * dt);// - this.findDragY();
        if (this.position.y >= Constants.SCREEN_HEIGHT || this.position.y < 0) {
            this.velocity.y = -this.velocity.y;
        }  
        if (this.position.x >= Constants.SCREEN_WIDTH || this.position.x < 0) {
            this.velocity.x = -this.velocity.x;
        }
    }

    private findArea() : number{
        return Math.PI * this.radius * this.radius;
    }

    private findVolume() : number{
        return (4/3) * Math.PI * this.radius * this.radius * this.radius;
    }

    private findDragX() : number {
        let drag = PhysicsConstants.COEFF_DRAG * 0.5 * PhysicsConstants.RHO_AIR * this.velocity.x * this.velocity.x * this.findArea();
        let direction = 1;
        if (this.velocity.x < 0) {
            direction = -1;
        }
        return direction * drag;
    }

    private findDragY() : number {
        let drag = PhysicsConstants.COEFF_DRAG * 0.5 * PhysicsConstants.RHO_AIR * this.velocity.y * this.velocity.y * this.findArea();
        let direction = 1;
        if (this.velocity.y < 0) {
            direction = -1;
        }
        return direction * drag;
    }

    private findBuoyancyForce() : number {
        return  this.findVolume() * PhysicsConstants.RHO_AIR * (-PhysicsConstants.G);
    }

    isColliding(shape: Shape): boolean {
        let isColliding = false;
        let xDist = Math.abs(shape.position.x - this.position.x);
        let yDist = Math.abs(shape.position.y - this.position.y);
        let distSquared = (xDist * xDist) + (yDist * yDist);
        if (distSquared <= (this.radius + shape.radius) * (this.radius + shape.radius)) { // Collision occurring
            isColliding = true;
        }
        return isColliding;
    }

    resolveCollision(shape: Shape) {
        
        debugger;

        let delta: Vector2D = (this.position.subtract(shape.position));
        let d = delta.length();
        // minimum translation distance to push balls apart after intersecting
        let mtd: Vector2D = delta.multiply(((this.radius + shape.radius)-d)/d); 
    
    
        // resolve intersection --
        // inverse mass quantities
        let im1 = 1 / this.mass; 
        let im2 = 1 / shape.mass;
    
        // push-pull them apart based off their mass
        this.position = this.position.add(mtd.multiply(im1 / (im1 + im2)));
        shape.position = shape.position.subtract(mtd.multiply(im2 / (im1 + im2)));
    
        // impact speed
        let v: Vector2D = (this.velocity.subtract(shape.velocity));
        let vn = v.dotProduct(mtd.normalize());
    
        // sphere intersecting but moving away from each other already
        if (vn > 0.0)  {
            return;
        }
    
        // collision impulse
        let i = (-( 1 + PhysicsConstants.COEFF_RESTITUTION) * vn) / (im1 + im2);
        let impulse = mtd.multiply(i);
    
        // change in momentum
        this.velocity = this.velocity.add(impulse.multiply(im1));
        shape.velocity = shape.velocity.subtract(impulse.multiply(im2));
    }

    // private findWeight(): number {
    //     let weightOfAir = this.findVolume() * PhysicsConstants.RHO_HELIUM * (PhysicsConstants.G);
    //     return PhysicsConstants.MASS_OF_BALLOON + weightOfAir;
    // }
    
    // private findWeight(): number {
    //     //let weightOfAir = this.findVolume() * PhysicsConstants.RHO_HELIUM * (PhysicsConstants.G);
    //     return this.mass;
    // }

 }