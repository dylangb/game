
export class Vector2D {

    public x;
    public y;

    public constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    // Compute magnitude of vector ....
    public length(): number {
        return Math.sqrt((this.x * this.x) + (this.y * this.y));
    }

    // Sum of two vectors ....

    public add(v1: Vector2D): Vector2D {
        let v2 = new Vector2D(this.x + v1.x, this.y + v1.y);
        return v2;
    }

    // Subtract vector v1 from v .....

    public subtract(v1: Vector2D): Vector2D {
        let v2 = new Vector2D(this.x - v1.x, this.y - v1.y);
        return v2;
    }

    // Scale vector by a constant ...

    public multiply(factor: number): Vector2D {
        let v2 = new Vector2D(this.x * factor, this.y * factor);
        return v2;
    }

    // Normalize a vectors length....

    public normalize(): Vector2D {
        let v2 = new Vector2D(0, 0);

        let length = Math.sqrt(this.x * this.x + this.y * this.y);
        if (length != 0) {
            v2.x = this.x / length;
            v2.y = this.y / length;
        }

        return v2;
    }

    // Dot product of two vectors .....

    public dotProduct(v1: Vector2D): number {
        return this.x * v1.x + this.y * v1.y;
    }

    public crossProduct(v1: Vector2D) {
        return this.x * v1.y - this.y * v1.x;
    }
}