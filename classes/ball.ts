import { Shape } from '../interfaces/shape'
import { PhysicsConstants } from '../physics.constants'
import { Constants } from '../constants'
import { Vector2D } from './vector2d'

export class Ball implements Shape {
    private isInContact: boolean = false;
    private timeInContact = 0;
    private impactForceY = 0;
    private deflectionY = 0;
    private deflectionVY = 0;
    private bounceAcceleration = 0;
    public mass: number = 0;
    public position: Vector2D;
    public velocity: Vector2D;
    public deformation: Vector2D;
    
    
    public radius: number = 10;
    public lineWidth: number = 2;
    public color: string = "red";
    constructor(x: number, y: number, radius: number, color: string = "red", line_width: number = 2)
    {
       this.position = new Vector2D(x, y);
       this.velocity = new Vector2D(0, 0);
       this.radius = radius;
       this.color = color;
       this.lineWidth = line_width;
    }

    public draw(ctx: CanvasRenderingContext2D) {
        if (!this.isInContact) {
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.lineWidth;
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.stroke();
            ctx.restore();
        } else {
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = 'red';
            ctx.lineWidth = this.lineWidth;
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.stroke();
            ctx.restore();
        }
        
    }

    public isColliding(shape: Shape): boolean {
        let isColliding = false;
        let xDist = Math.abs(shape.position.x - this.position.x);
        let yDist = Math.abs(shape.position.y - this.position.y);
        let distSquared = (xDist * xDist) + (yDist * yDist);
        if (distSquared <= (this.radius + shape.radius) * (this.radius + shape.radius)) { // Collision occurring
            isColliding = true;
        }
        return isColliding;
    }

    resolveCollision(shape: Shape) {
        debugger;
        let delta: Vector2D = (this.position.subtract(shape.position));
        let d = delta.length();
        // minimum translation distance to push balls apart after intersecting
        let mtd: Vector2D = delta.multiply(((this.radius + shape.radius)-d)/d); 
    
    
        // resolve intersection --
        // inverse mass quantities
        let im1 = 1 / this.mass; 
        let im2 = 1 / shape.mass;
    
        // push-pull them apart based off their mass
        this.position = this.position.add(mtd.multiply(im1 / (im1 + im2)));
        shape.position = shape.position.subtract(mtd.multiply(im2 / (im1 + im2)));
    
        // impact speed
        let v: Vector2D = (this.velocity.subtract(shape.velocity));
        let vn = v.dotProduct(mtd.normalize());
    
        // sphere intersecting but moving away from each other already
        if (vn > 0.0)  {
            return;
        }
    
        // collision impulse
        let i = (-(PhysicsConstants.COEFF_RESTITUTION) * vn) / (im1 + im2);
        let impulse = mtd.multiply(i);
    
        // change in momentum
        this.velocity = this.velocity.add(impulse.multiply(im1));
        shape.velocity = shape.velocity.subtract(impulse.multiply(im2));
    }

    public updatePosition(dt: number) {

        if (this.position.y < Constants.SCREEN_HEIGHT && !this.isInContact) {
            this.velocity.y +=  PhysicsConstants.G * dt;
            this.position.x += this.velocity.x * dt
            this.position.y += this.velocity.y * dt;
        } else if (this.position.y >= Constants.SCREEN_HEIGHT && this.velocity.y > 0 && !this.isInContact ) {
            this.isInContact = true;
            this.deflectionY = 0;
            this.deflectionVY = this.velocity.y;
            this.velocity.y = - (PhysicsConstants.COEFF_RESTITUTION_HOPPER * this.velocity.y);
            this.position.y = Constants.SCREEN_HEIGHT - 0.000001; 
            
        }
        
        if (this.isInContact) {

            this.bounceAcceleration = (-PhysicsConstants.STIFFNESS_OF_HOPPER / PhysicsConstants.MASS_OF_HOPPER) * this.deflectionY;
            this.deflectionVY += this.bounceAcceleration * dt;
            this.deflectionY += (this.deflectionVY * dt) + (0.5 * this.bounceAcceleration * dt * dt);
            if (this.deflectionY < 0) {
                this.isInContact = false;
            }
        }
    }

    private findArea() : number{
        return Math.PI * this.radius * this.radius;
    }

    private findVolume() : number{
        return (4/3) * Math.PI * this.radius * this.radius * this.radius;
    }

    private findDragX() : number {
        return PhysicsConstants.COEFF_DRAG * 0.5 * PhysicsConstants.RHO_AIR * this.velocity.x * this.velocity.x * this.findArea();
    }

    private findDragY() : number {
        return PhysicsConstants.COEFF_DRAG * 0.5 * PhysicsConstants.RHO_AIR * this.velocity.y * this.velocity.y * this.findArea();
    }

    private findBuoyancyForce() : number {
        return  this.findVolume() * PhysicsConstants.RHO_AIR * (-PhysicsConstants.G);
    }

    // private findWeight(): number {
    //     let weightOfAir = this.findVolume() * PhysicsConstants.RHO_HELIUM * (PhysicsConstants.G);
    //     return PhysicsConstants.MASS_OF_BALLOON + weightOfAir;
    // }
    
    // private findWeight(): number {
    //     //let weightOfAir = this.findVolume() * PhysicsConstants.RHO_HELIUM * (PhysicsConstants.G);
    //     return this.mass;
    // }

 }