import { Shape } from '../interfaces/shape'
import { PhysicsConstants } from '../physics.constants'
import { Constants } from '../constants'

export class Circle {
    public vX: number = 0;
    public vY: number = 0;
    public x: number = 0;
    public y: number = 0;
    public radius: number = 10;
    public lineWidth: number = 2;
    public color: string = "red";
    constructor(x: number, y: number, radius: number, color: string = "red", line_width: number = 2)
    {
       this.x = x;
       this.y = y;
       this.radius = radius;
       this.color = color;
       this.lineWidth = line_width;
    }

    public updatePosition(dt: number) {
        this.vY +=  PhysicsConstants.G * dt;
        this.x += this.vX * dt
        this.y += this.vY * dt;
        if (this.y >= Constants.SCREEN_HEIGHT || this.y < 0) {
            this.vY = -this.vY;
        }  
    }


    public draw = (ctx: CanvasRenderingContext2D): void => {
       ctx.save();
       ctx.beginPath();
       ctx.strokeStyle = this.color;
       ctx.lineWidth = this.lineWidth;
       ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
       ctx.stroke();
       ctx.restore();
    }
 }