var nodeExternals = require('webpack-node-externals');
const path = require('path');
module.exports = { 
    watch: true, 
    entry: './app.ts',
    output: {
      filename: 'bundle.js'
    },
    resolve: {
      extensions: ['.webpack.js', '.web.js', '.ts', '.js']
    },

    module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: [/node_modules/]
          }
        ],
        loaders: [
            {

              exclude: [/node_modules/],
            }
          ]
      },

    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    externals: [nodeExternals()], 
  }