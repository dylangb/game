/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Constants;
(function (Constants) {
    Constants.SCREEN_WIDTH = 600;
    Constants.SCREEN_HEIGHT = 700;
    Constants.FPS = 130 / 1000;
    Constants.SCREEN_BASE_CLOUR = "white";
    Constants.DETECTION_LIMIT = 0.002;
})(Constants = exports.Constants || (exports.Constants = {}));


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var PhysicsConstants;
(function (PhysicsConstants) {
    PhysicsConstants.G = 9.81;
    PhysicsConstants.RHO_AIR = 1.23; // kg / m ^ 3
    PhysicsConstants.RHO_HELIUM = 0.000000164; // kg / m ^ 3 
    PhysicsConstants.COEFF_DRAG = 0.0000001; // Roughly
    PhysicsConstants.MASS_OF_BALLOON = 0.012; // kg Roughly
    PhysicsConstants.MASS_OF_HOPPER = 0.4; // kg Roughly
    PhysicsConstants.STIFFNESS_OF_HOPPER = 100; // Stiffness units... (k) Hookes Law
    PhysicsConstants.BOUNCE_DURATION = 1.0; // seconds Complete guess should be calculated from stiffness
    PhysicsConstants.COEFF_RESTITUTION = 0.0; // Coeffecient of restitution between balloons
    PhysicsConstants.COEFF_RESTITUTION_HOPPER = 0.5; // Coeffecient of restitution between balloons
})(PhysicsConstants = exports.PhysicsConstants || (exports.PhysicsConstants = {}));


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Vector2D {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    // Compute magnitude of vector ....
    length() {
        return Math.sqrt((this.x * this.x) + (this.y * this.y));
    }
    // Sum of two vectors ....
    add(v1) {
        let v2 = new Vector2D(this.x + v1.x, this.y + v1.y);
        return v2;
    }
    // Subtract vector v1 from v .....
    subtract(v1) {
        let v2 = new Vector2D(this.x - v1.x, this.y - v1.y);
        return v2;
    }
    // Scale vector by a constant ...
    multiply(factor) {
        let v2 = new Vector2D(this.x * factor, this.y * factor);
        return v2;
    }
    // Normalize a vectors length....
    normalize() {
        let v2 = new Vector2D(0, 0);
        let length = Math.sqrt(this.x * this.x + this.y * this.y);
        if (length != 0) {
            v2.x = this.x / length;
            v2.y = this.y / length;
        }
        return v2;
    }
    // Dot product of two vectors .....
    dotProduct(v1) {
        return this.x * v1.x + this.y * v1.y;
    }
    crossProduct(v1) {
        return this.x * v1.y - this.y * v1.x;
    }
}
exports.Vector2D = Vector2D;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Utils {
    static randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
}
exports.Utils = Utils;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const ball_1 = __webpack_require__(5);
const balloon_1 = __webpack_require__(6);
const constants_1 = __webpack_require__(0);
const input_1 = __webpack_require__(7);
const utils_1 = __webpack_require__(3);
let canvas;
let ctx;
let shape = new ball_1.Ball(20, 0, 80, 'blue', 0.6);
let frameCount = 0;
let particle1 = new balloon_1.Balloon(200, 10, utils_1.Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle2 = new balloon_1.Balloon(100, 120, utils_1.Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle4 = new balloon_1.Balloon(150, 330, utils_1.Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle5 = new balloon_1.Balloon(200, 430, utils_1.Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particle6 = new balloon_1.Balloon(300, 500, utils_1.Utils.randomIntFromInterval(20, 60), 'green', 0.6);
let particleList = [];
function gameLoop() {
    requestAnimationFrame(gameLoop);
    ctx.fillStyle = constants_1.Constants.SCREEN_BASE_CLOUR;
    ctx.fillRect(0, 0, constants_1.Constants.SCREEN_WIDTH, constants_1.Constants.SCREEN_HEIGHT);
    resolveCollisions(particleList);
    updateParticles(particleList);
    drawParticles(particleList, ctx);
    shape.updatePosition(constants_1.Constants.FPS); //
    shape.draw(ctx);
    frameCount++;
}
window.onload = () => {
    particleList.push(particle1);
    particleList.push(particle2);
    // particleList.push(particle3);
    particleList.push(particle4);
    particleList.push(particle5);
    particleList.push(particle6);
    canvas = document.getElementById('cnvs');
    ctx = canvas.getContext("2d");
    document.addEventListener('keydown', function (event) {
        input_1.Input.keyboardInput(event, shape);
    });
    frameCount = 0;
    gameLoop();
};
function resolveCollisions(particleList) {
    //debugger;
    for (let i = 0; i < particleList.length; i++) {
        for (let j = i + 1; j < particleList.length; j++) {
            if (particleList[i].isColliding(particleList[j])) {
                particleList[i].resolveCollision(particleList[j]);
                //console.log('p:' + i + ', x: ' + particleList[i].position.x + ', y: ' + particleList[i].position.x + ', vX: ' + particleList[i].position.x + ', vY: ' + particleList[i].position.x);
            }
        }
    }
}
function updateParticles(particleList) {
    for (let shape of particleList) {
        shape.updatePosition(constants_1.Constants.FPS);
    }
}
function drawParticles(particleList, ctx) {
    for (let shape of particleList) {
        shape.draw(ctx);
    }
}


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const physics_constants_1 = __webpack_require__(1);
const constants_1 = __webpack_require__(0);
const vector2d_1 = __webpack_require__(2);
class Ball {
    constructor(x, y, radius, color = "red", line_width = 2) {
        this.isInContact = false;
        this.timeInContact = 0;
        this.impactForceY = 0;
        this.deflectionY = 0;
        this.deflectionVY = 0;
        this.bounceAcceleration = 0;
        this.mass = 0;
        this.radius = 10;
        this.lineWidth = 2;
        this.color = "red";
        this.position = new vector2d_1.Vector2D(x, y);
        this.velocity = new vector2d_1.Vector2D(0, 0);
        this.radius = radius;
        this.color = color;
        this.lineWidth = line_width;
    }
    draw(ctx) {
        if (!this.isInContact) {
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.lineWidth;
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.stroke();
            ctx.restore();
        }
        else {
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = 'red';
            ctx.lineWidth = this.lineWidth;
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.stroke();
            ctx.restore();
        }
    }
    isColliding(shape) {
        let isColliding = false;
        let xDist = Math.abs(shape.position.x - this.position.x);
        let yDist = Math.abs(shape.position.y - this.position.y);
        let distSquared = (xDist * xDist) + (yDist * yDist);
        if (distSquared <= (this.radius + shape.radius) * (this.radius + shape.radius)) {
            isColliding = true;
        }
        return isColliding;
    }
    resolveCollision(shape) {
        debugger;
        let delta = (this.position.subtract(shape.position));
        let d = delta.length();
        // minimum translation distance to push balls apart after intersecting
        let mtd = delta.multiply(((this.radius + shape.radius) - d) / d);
        // resolve intersection --
        // inverse mass quantities
        let im1 = 1 / this.mass;
        let im2 = 1 / shape.mass;
        // push-pull them apart based off their mass
        this.position = this.position.add(mtd.multiply(im1 / (im1 + im2)));
        shape.position = shape.position.subtract(mtd.multiply(im2 / (im1 + im2)));
        // impact speed
        let v = (this.velocity.subtract(shape.velocity));
        let vn = v.dotProduct(mtd.normalize());
        // sphere intersecting but moving away from each other already
        if (vn > 0.0) {
            return;
        }
        // collision impulse
        let i = (-(physics_constants_1.PhysicsConstants.COEFF_RESTITUTION) * vn) / (im1 + im2);
        let impulse = mtd.multiply(i);
        // change in momentum
        this.velocity = this.velocity.add(impulse.multiply(im1));
        shape.velocity = shape.velocity.subtract(impulse.multiply(im2));
    }
    updatePosition(dt) {
        if (this.position.y < constants_1.Constants.SCREEN_HEIGHT && !this.isInContact) {
            this.velocity.y += physics_constants_1.PhysicsConstants.G * dt;
            this.position.x += this.velocity.x * dt;
            this.position.y += this.velocity.y * dt;
        }
        else if (this.position.y >= constants_1.Constants.SCREEN_HEIGHT && this.velocity.y > 0 && !this.isInContact) {
            this.isInContact = true;
            this.deflectionY = 0;
            this.deflectionVY = this.velocity.y;
            this.velocity.y = -(physics_constants_1.PhysicsConstants.COEFF_RESTITUTION_HOPPER * this.velocity.y);
            this.position.y = constants_1.Constants.SCREEN_HEIGHT - 0.000001;
        }
        if (this.isInContact) {
            this.bounceAcceleration = (-physics_constants_1.PhysicsConstants.STIFFNESS_OF_HOPPER / physics_constants_1.PhysicsConstants.MASS_OF_HOPPER) * this.deflectionY;
            this.deflectionVY += this.bounceAcceleration * dt;
            this.deflectionY += (this.deflectionVY * dt) + (0.5 * this.bounceAcceleration * dt * dt);
            if (this.deflectionY < 0) {
                this.isInContact = false;
            }
        }
    }
    findArea() {
        return Math.PI * this.radius * this.radius;
    }
    findVolume() {
        return (4 / 3) * Math.PI * this.radius * this.radius * this.radius;
    }
    findDragX() {
        return physics_constants_1.PhysicsConstants.COEFF_DRAG * 0.5 * physics_constants_1.PhysicsConstants.RHO_AIR * this.velocity.x * this.velocity.x * this.findArea();
    }
    findDragY() {
        return physics_constants_1.PhysicsConstants.COEFF_DRAG * 0.5 * physics_constants_1.PhysicsConstants.RHO_AIR * this.velocity.y * this.velocity.y * this.findArea();
    }
    findBuoyancyForce() {
        return this.findVolume() * physics_constants_1.PhysicsConstants.RHO_AIR * (-physics_constants_1.PhysicsConstants.G);
    }
}
exports.Ball = Ball;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const vector2d_1 = __webpack_require__(2);
const physics_constants_1 = __webpack_require__(1);
const constants_1 = __webpack_require__(0);
const utils_1 = __webpack_require__(3);
class Balloon {
    constructor(x, y, radius, color = "red", line_width = 2) {
        this.radius = 10;
        this.lineWidth = 2;
        this.color = "red";
        this.position = new vector2d_1.Vector2D(x, y);
        this.velocity = new vector2d_1.Vector2D(utils_1.Utils.randomIntFromInterval(1, 20), utils_1.Utils.randomIntFromInterval(1, 20));
        this.mass = this.findVolume() * physics_constants_1.PhysicsConstants.RHO_HELIUM;
        this.radius = radius;
        this.color = color;
        this.lineWidth = line_width;
    }
    draw(ctx) {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = this.color;
        ctx.lineWidth = this.lineWidth;
        ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.restore();
    }
    updatePosition(dt) {
        //this.velocity.y +=  PhysicsConstants.G * dt;
        //debugger;
        let dragX = this.findDragX();
        let dragY = this.findDragY();
        this.position.x += (this.velocity.x * dt); // - this.findDragX();
        this.position.y += (this.velocity.y * dt); // - this.findDragY();
        if (this.position.y >= constants_1.Constants.SCREEN_HEIGHT || this.position.y < 0) {
            this.velocity.y = -this.velocity.y;
        }
        if (this.position.x >= constants_1.Constants.SCREEN_WIDTH || this.position.x < 0) {
            this.velocity.x = -this.velocity.x;
        }
    }
    findArea() {
        return Math.PI * this.radius * this.radius;
    }
    findVolume() {
        return (4 / 3) * Math.PI * this.radius * this.radius * this.radius;
    }
    findDragX() {
        let drag = physics_constants_1.PhysicsConstants.COEFF_DRAG * 0.5 * physics_constants_1.PhysicsConstants.RHO_AIR * this.velocity.x * this.velocity.x * this.findArea();
        let direction = 1;
        if (this.velocity.x < 0) {
            direction = -1;
        }
        return direction * drag;
    }
    findDragY() {
        let drag = physics_constants_1.PhysicsConstants.COEFF_DRAG * 0.5 * physics_constants_1.PhysicsConstants.RHO_AIR * this.velocity.y * this.velocity.y * this.findArea();
        let direction = 1;
        if (this.velocity.y < 0) {
            direction = -1;
        }
        return direction * drag;
    }
    findBuoyancyForce() {
        return this.findVolume() * physics_constants_1.PhysicsConstants.RHO_AIR * (-physics_constants_1.PhysicsConstants.G);
    }
    isColliding(shape) {
        let isColliding = false;
        let xDist = Math.abs(shape.position.x - this.position.x);
        let yDist = Math.abs(shape.position.y - this.position.y);
        let distSquared = (xDist * xDist) + (yDist * yDist);
        if (distSquared <= (this.radius + shape.radius) * (this.radius + shape.radius)) {
            isColliding = true;
        }
        return isColliding;
    }
    resolveCollision(shape) {
        debugger;
        let delta = (this.position.subtract(shape.position));
        let d = delta.length();
        // minimum translation distance to push balls apart after intersecting
        let mtd = delta.multiply(((this.radius + shape.radius) - d) / d);
        // resolve intersection --
        // inverse mass quantities
        let im1 = 1 / this.mass;
        let im2 = 1 / shape.mass;
        // push-pull them apart based off their mass
        this.position = this.position.add(mtd.multiply(im1 / (im1 + im2)));
        shape.position = shape.position.subtract(mtd.multiply(im2 / (im1 + im2)));
        // impact speed
        let v = (this.velocity.subtract(shape.velocity));
        let vn = v.dotProduct(mtd.normalize());
        // sphere intersecting but moving away from each other already
        if (vn > 0.0) {
            return;
        }
        // collision impulse
        let i = (-(1 + physics_constants_1.PhysicsConstants.COEFF_RESTITUTION) * vn) / (im1 + im2);
        let impulse = mtd.multiply(i);
        // change in momentum
        this.velocity = this.velocity.add(impulse.multiply(im1));
        shape.velocity = shape.velocity.subtract(impulse.multiply(im2));
    }
}
exports.Balloon = Balloon;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Input {
    static keyboardInput(event, shape) {
        // PRESS LEFT ARROW OR 'A' KEY
        if (event.keyCode == 37 || event.keyCode == 65) {
            shape.velocity.x -= 5;
        }
        else if (event.keyCode == 38 || event.keyCode == 87) {
            shape.velocity.y -= 5;
        }
        else if (event.keyCode == 39 || event.keyCode == 68) {
            shape.velocity.x += 5;
        }
        else if (event.keyCode == 40 || event.keyCode == 83) {
            shape.velocity.y += 5;
        }
    }
}
exports.Input = Input;


/***/ })
/******/ ]);