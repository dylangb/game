export module Constants {
    export var SCREEN_WIDTH = 600;
    export var SCREEN_HEIGHT = 700;
    export var FPS = 130 / 1000;
    export var SCREEN_BASE_CLOUR = "white";
    export var DETECTION_LIMIT = 0.002;
}