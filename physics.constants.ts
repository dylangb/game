export module PhysicsConstants {
    export var G = 9.81;
    export var RHO_AIR = 1.23; // kg / m ^ 3
    export var RHO_HELIUM = 0.000000164; // kg / m ^ 3 
    export var COEFF_DRAG = 0.0000001; // Roughly
    export var MASS_OF_BALLOON = 0.012; // kg Roughly
    export var MASS_OF_HOPPER = 0.4; // kg Roughly
    export var STIFFNESS_OF_HOPPER = 100; // Stiffness units... (k) Hookes Law
    export var BOUNCE_DURATION = 1.0; // seconds Complete guess should be calculated from stiffness
    export var COEFF_RESTITUTION = 0.0; // Coeffecient of restitution between balloons
    export var COEFF_RESTITUTION_HOPPER = 0.5; // Coeffecient of restitution between balloons
}