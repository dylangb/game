"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const circle_1 = require("./classes/circle");
var canvas;
var ctx;
var circle1 = new circle_1.Circle(200, 300, 50);
var circle2 = new circle_1.Circle(400, 550, 150, "blue", 5);
window.onload = () => {
    canvas = document.getElementById('cnvs');
    ctx = canvas.getContext("2d");
    gameLoop();
};
function gameLoop() {
    requestAnimationFrame(gameLoop);
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, 1280, 720);
    ctx.beginPath();
    ctx.strokeStyle = "red";
    ctx.lineWidth = 5;
    ctx.arc(400, 400, 100, 0, 2 * Math.PI);
    ctx.stroke();
    circle1.draw(ctx);
    circle2.draw(ctx);
}
//# sourceMappingURL=app.js.map