import { Vector2D } from '../classes/vector2d'

export interface Shape {
    draw(ctx: CanvasRenderingContext2D): void;
    updatePosition(dt: number);
    isColliding(shape: Shape): boolean;
    resolveCollision(shape: Shape);
    position: Vector2D;
    velocity: Vector2D;
    deformation: Vector2D;
    mass: number;
    radius: number;
    color: string;
    lineWidth: number;
 }