export interface Shape {
    draw(ctx: CanvasRenderingContext2D): void;
    updatePosition(dt: number);
    x: number;
    y: number;
    color: string;
    lineWidth: number;
 }