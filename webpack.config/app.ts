import { Circle } from './circle';
import { Shape } from './shape';

let canvas: HTMLCanvasElement;
let ctx: CanvasRenderingContext2D;
let circle: Shape = new Circle(20, 20, 2, 'blue', 0.2);
let frameCount = 0;

function gameLoop() {
    requestAnimationFrame(gameLoop);
    circle.updatePosition(60 / 1000);
    circle.draw(ctx);
    frameCount++;
}

window.onload = () => {
    canvas = <HTMLCanvasElement>document.getElementById('cnvs');
    ctx = canvas.getContext("2d");
    frameCount = 0;
    //this.
    gameLoop();
}


